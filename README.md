# NESTJS Microservice
Contoh Implementasi Microservice pada Nest JS menggunakan EventPattern dan MessagePattern. Aplikasi berisikan tentang subsciber dengan data terdiri dari id, email dan name.

Sistem terdiri dari dua NestJS Projek berupa: 

- nestjs-email-subscriptions-client
- nestjs-email-subscriptions-server

## nestjs-email-subscriptions-client
Implementasi Client dari Microservice yang menerima request HTTP serta berperan sebagai perantara dari client kepada Server yang bekerja dalam Transport TCP. 

- host          : localhost
- port          : 7007
- listening TCP : 7006

## nestjs-email-subscriptions-server
Implementasi Server dari Microservice yang menerima request TCP serta terkoneksi ke postgres database. 

- host          : localhost
- port          : 7006
- DB_PORT       : 5401

## Running Project

```sh
# clone repository
git clone https://gitlab.com/hilmanfitriana19/nest-js-microservice.git

# setup database
# copy sample.database.env 
cp sample.database.env .database.env

# run postgres db
docker-compose up

# setup server app
# copy .env file dari sample.env
cd nestjs-email-subscriptions-server
cp sample.env .env

#install dependecy
npm install

#run app
npm run start


# setup client app
cd nestjs-email-subscriptions-server

#install dependecy
npm install

#run app
npm run start

```


## API Endpoint
API Endpoint dapat diakses pada route path home `\` dengan structure data berupa 

``` JSON
{
    "name": "test",
    "email":"test@test.com"
}
```


#### Endpoint
| Request Method     |  Fungsi    |
| :-------- | :--------------------------------------- |
| `[GET] \` |  `GET List Subscriber` |
| `[GET] \:id` |  `GET Subscriber with ID` |
| `[POST] \:id` |  `CREATE Subscriber` |
| `[PUT] \:id` |  `UPDATE Subscriber` |
| `[DELETE] \:id` |  `DELETE Subscriber` |
