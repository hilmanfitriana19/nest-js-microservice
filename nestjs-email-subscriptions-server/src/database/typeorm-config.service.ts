/* eslint-disable prettier/prettier */
import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmOptionsFactory, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Subscribers } from '../subscribers/entities/subscribers.entity';

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  @Inject(ConfigService)
  private readonly config: ConfigService;

  public createTypeOrmOptions(): TypeOrmModuleOptions {

    console.log(this.config.get<number>('DATABASE_PORT'))
    return {
      type: this.config.get<any>('DATABASE_TYPE'),
      host: this.config.get<string>('DATABASE_HOST'),
      port: this.config.get<number>('DATABASE_PORT'),
      database: this.config.get<string>('DATABASE_DB'),
      username: this.config.get<string>('DATABASE_USER'),
      password: this.config.get<string>('DATABASE_PASSWORD'),
      entities: [Subscribers],
      retryAttempts: 20, //jumlah percobaan untuk connect database
      retryDelay: 5000, //jeda waktu antar percobaan connect database
      logging: false,
      synchronize:false
    };
  }
}