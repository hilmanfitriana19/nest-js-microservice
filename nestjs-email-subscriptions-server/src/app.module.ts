import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config/dist';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { TypeOrmConfigService } from './database/typeorm-config.service';
import { SubscribersModule } from './subscribers/subscribers.module';

@Module({
  imports: [
    SubscribersModule,
    ConfigModule.forRoot({isGlobal:true}),
    TypeOrmModule.forRootAsync({useClass:TypeOrmConfigService}),
  ],
})
export class AppModule {}
