import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('my_subscribers')
export class Subscribers {
    @PrimaryGeneratedColumn()
    public id:number;

    @Column({unique:true})
    public email: string;

    @Column()
    public name: string;
}
