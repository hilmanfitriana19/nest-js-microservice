
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSubscribersDto } from './dto/create-subscribers.dto';
import { Subscribers } from './entities/subscribers.entity';
 
@Injectable()
export class SubscribersService {
  constructor(
    @InjectRepository(Subscribers)
    private subscribersRepository: Repository<Subscribers>,
  ) {}
 

  async getSubscribers(id: number) {
    return this.subscribersRepository.findOneBy({id});
  }
  
  async addSubscriber(subscriber: CreateSubscribersDto) {
    const newSubscriber = await this.subscribersRepository.create(subscriber);
    await this.subscribersRepository.save(newSubscriber);
  }
 
  async getAllSubscribers() {
    return this.subscribersRepository.find();
  }

  deleteSubscriber(id: number) {
    return this.subscribersRepository.delete(id)
  }
  async updateSubscribers(id: number, subscriber: CreateSubscribersDto) {
    return this.subscribersRepository.update(id,subscriber);
    
  }

}