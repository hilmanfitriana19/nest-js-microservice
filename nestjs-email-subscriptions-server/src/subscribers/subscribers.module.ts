import { Module } from '@nestjs/common';
import { SubscribersService } from './subscribers.service';
import { SubscribersController } from './subscribers.controller';
import { TypeOrmModule } from '@nestjs/typeorm/dist/typeorm.module';
import { Subscribers } from './entities/subscribers.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Subscribers])],
  controllers: [SubscribersController],
  providers: [SubscribersService]
})
export class SubscribersModule {}
