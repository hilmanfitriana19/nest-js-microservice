import { PartialType } from '@nestjs/mapped-types';
import { CreateSubscribersDto } from './create-subscribers.dto';

export class UpdateSubscribersDto extends PartialType(CreateSubscribersDto) {}
