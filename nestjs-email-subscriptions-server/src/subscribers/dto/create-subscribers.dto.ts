import { IsEmail, IsString } from "class-validator";

export class CreateSubscribersDto {
    @IsString()
    public name:string;

    @IsString()
    @IsEmail()
    public email:string;
}
