
import { Controller } from '@nestjs/common';
import { EventPattern, MessagePattern } from '@nestjs/microservices';
import { CreateSubscribersDto } from './dto/create-subscribers.dto';
import { SubscribersService } from './subscribers.service';

 
@Controller('subscribers')
export class SubscribersController {
  constructor(
    private readonly subscribersService: SubscribersService,
  ) {}
 
  @EventPattern('add-subscriber' )
  addSubscriber(subscribers: CreateSubscribersDto) {
    return this.subscribersService.addSubscriber(subscribers);
  }
 
  @MessagePattern({ cmd: 'get-all-subscriber' })
  getAllSubscribers() {
    return this.subscribersService.getAllSubscribers();
  }

  @MessagePattern({ cmd: 'get-a-subscriber', })
  getSubscriber(id: number) {
    return this.subscribersService.getSubscribers(id);
  }

  @EventPattern('update-subscriber' )
  updateSubscriber(data) {
    return this.subscribersService.updateSubscribers(data.id,data.body);
  }

  @EventPattern('delete-subscriber' )
  deleteSubscriber(id: number) {
    return this.subscribersService.deleteSubscriber(id);
  }


}