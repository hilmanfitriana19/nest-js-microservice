import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateSubscribersDto } from './create-subscribers.dto';

@Injectable()
export class AppService {
  
  constructor(
    @Inject('TCP_SERVER') private readonly tcpClient: ClientProxy
  ){}

  getListSubscriber() {
    return this.tcpClient.send({cmd:'get-all-subscriber'},{});
  }

  updateSubscriber(id: number, body: CreateSubscribersDto) {
    return this.tcpClient.emit('update-subscriber',{id,body});
  }

  deleteSubscriber(id: number) {
    return this.tcpClient.emit('delete-subscriber',id);
  }

  getSubscriber(id: number) {
    return this.tcpClient.send({cmd:'get-a-subscriber'},id);
  }

  createSubscriber(body : CreateSubscribersDto) {
    return this.tcpClient.emit('add-subscriber',body);
  }
}
