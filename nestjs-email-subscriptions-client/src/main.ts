import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  await app.listen(7007,()=>{
    console.log('Listen in ',7007);
  });
}
bootstrap();
