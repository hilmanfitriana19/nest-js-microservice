import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { AppService } from './app.service';
import { CreateSubscribersDto } from './create-subscribers.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getListSubscriber() {
    return this.appService.getListSubscriber();
  }

  @Post()
  createSubscriber(@Body() body: CreateSubscribersDto) {
    return this.appService.createSubscriber(body);
  }

  @Get(':id')
  getSubscriber(@Param('id') id:number) {
    return this.appService.getSubscriber(id);
  }

  @Put(':id')
  updateSubscriber(@Param('id')id:number, @Body() body: CreateSubscribersDto) {
    return this.appService.updateSubscriber(id,body);
  }

  @Delete(':id')
  deleteSubscriver(@Param('id')id:number){
    return this.appService.deleteSubscriber(id);
  }
}
